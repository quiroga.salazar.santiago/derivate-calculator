package finalproject.calculo.derivatecalculator.operations;

import finalproject.calculo.derivatecalculator.common.MyFunction;

public class Root implements MyFunction {
  MyFunction f;
  int root;

  public Root(MyFunction f, int root) {
    this.f = f;
    this.root = root;
  }

  @Override
  public String getResult() {
    return root > 2 ? "(%s)/(%d(%d√(%s)^%d))".formatted(f.getResult(), root, root, f.getFunction(), root - 1) :
            "(%s)/(%d(%d√(%s)))".formatted(f.getResult(), root, root, f.getFunction()) ;
  }
}
