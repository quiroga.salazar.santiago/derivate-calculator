package finalproject.calculo.derivatecalculator.operations;

import finalproject.calculo.derivatecalculator.common.MyFunction;

public class Division implements MyFunction {
  MyFunction f;
  MyFunction g;

  public Division(MyFunction f, MyFunction g) {
    this.f = f;
    this.g = g;
  }

  @Override
  public String getResult() {
    return "( (%s) * (%s) - (%s) * (%s) ) / (%s)²"
            .formatted(f.getResult(), g.getFunction(), f.getFunction(), g.getResult(), g.getFunction());
  }
}
