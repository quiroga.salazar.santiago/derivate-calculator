package finalproject.calculo.derivatecalculator.operations;

import finalproject.calculo.derivatecalculator.common.MyFunction;

public class Multiplication implements MyFunction {
  private final MyFunction f;
  private final MyFunction g;

  public Multiplication(MyFunction f, MyFunction g) {
    this.f = f;
    this.g = g;
  }

  @Override
  public String getResult() {
    return "(%s) * (%s) + (%s) * (%s)".formatted(f.getResult(), g.getFunction(), f.getFunction(), g.getResult());
  }
}
