package finalproject.calculo.derivatecalculator.operations;

import finalproject.calculo.derivatecalculator.common.MyFunction;

public class SumRest implements MyFunction {
  private final MyFunction f;
  private final MyFunction g;

  public SumRest(MyFunction f, MyFunction g){
    this.f = f;
    this.g = g;
  }

  @Override
  public String getResult() {
    return "(%s) +- (%s)".formatted(f.getResult(), g.getResult());
  }
}
