package finalproject.calculo.derivatecalculator.functions;

import finalproject.calculo.derivatecalculator.common.MyFunction;

import java.text.MessageFormat;

public class Trinomial implements MyFunction {
  private final int a;
  private final int n;
  private final int b;
  private final int m;
  private final int c;

  public Trinomial(int a, int n, int b, int m, int c) {
    this.a = a;
    this.n = n;
    this.b = b;
    this.m = m;
    this.c = c;
  }

  @Override
  public String getResult() {
    int funcA = a*n;
    int funcN = n - 1;
    int funcB = b*m;
    int funcM = m - 1;
    int funcC = (c/c) - 1;
    return funcM >= 1 ?
            MessageFormat.format("{0}x^{1} + {2}x^{3} + {4}", funcA, funcN, funcB, funcM, funcC) :
            MessageFormat.format("{0}x^{1} + {2} + {3}", funcA, funcN, funcB, funcC);
  }

  @Override
  public String getFunction() {
    return "%dx^%d + %dx^%d + %d".formatted(a, n, b, m, c);
  }
}
