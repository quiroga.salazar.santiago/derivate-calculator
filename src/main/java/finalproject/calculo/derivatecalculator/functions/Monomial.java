package finalproject.calculo.derivatecalculator.functions;

import finalproject.calculo.derivatecalculator.common.MyFunction;

public class Monomial implements MyFunction {
  private int c;

  public Monomial(int c) {
    this.c = c;
  }

  public void setMonomial(int c) {
    this.c = c;
  }

  @Override
  public String getResult() {
    int result = c/c;
    result--;
    return String.valueOf(result);
  }

  @Override
  public String getFunction() {
    return String.valueOf(this.c);
  }
}
