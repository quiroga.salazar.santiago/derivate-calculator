package finalproject.calculo.derivatecalculator.functions;

import finalproject.calculo.derivatecalculator.common.MyFunction;

import java.text.MessageFormat;

public class Binomial implements MyFunction {
  private final int a;
  private final int n;
  private final int c;

  public Binomial(int a, int n, int c) {
    this.a = a;
    this.n = n;
    this.c = c;
  }

  @Override
  public String getResult() {
    int funcA = a*n;
    int funcN = n -1 ;
    int funcC = (c/c) - 1;
    return funcN >= 1 ?
            MessageFormat.format("{0}x^{1} + {2}", funcA, funcN, funcC) :
            MessageFormat.format("{0} + 0", funcA);
  }

  @Override
  public String getFunction() {
    return "%dx^%d + %d".formatted(a, n, c);
  }
}
