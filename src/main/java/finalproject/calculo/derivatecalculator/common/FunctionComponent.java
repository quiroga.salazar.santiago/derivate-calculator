package finalproject.calculo.derivatecalculator.common;

import javafx.scene.Node;
import javafx.scene.layout.HBox;

public interface FunctionComponent {
  default String getTitle() {
    return null;
  }

  default HBox getHBox() {
    return null;
  }

  Node build();

  MyFunction confirmClicked();
}
