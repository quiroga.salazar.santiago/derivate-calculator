package finalproject.calculo.derivatecalculator.common;

import javafx.scene.Node;

public interface MyFunction {
  String getResult();

  default String getFunction() {
    return getResult();
  }

}
