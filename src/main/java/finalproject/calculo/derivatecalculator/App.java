package finalproject.calculo.derivatecalculator;

import finalproject.calculo.derivatecalculator.ui.organisms.Dashboard;
import javafx.application.Application;
import javafx.stage.Stage;

public class App extends Application {
  @Override
  public void start(Stage primaryStage) {
    new Dashboard().showComponent();
  }

  public static void main(String[] args) {
    launch(args);
  }
}
