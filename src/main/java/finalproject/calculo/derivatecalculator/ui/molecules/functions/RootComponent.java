package finalproject.calculo.derivatecalculator.ui.molecules.functions;

import finalproject.calculo.derivatecalculator.common.FunctionComponent;
import finalproject.calculo.derivatecalculator.common.MyFunction;
import finalproject.calculo.derivatecalculator.operations.Root;
import finalproject.calculo.derivatecalculator.ui.atoms.AppTextField;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class RootComponent implements FunctionComponent {
  private final TextField field;
  private final HBox fields;
  private final Label title;
  private final VBox equation;
  private final FunctionComponent fX;

  public RootComponent(FunctionComponent fX) {
    this.field = new AppTextField("n");
    this.title = new Label("n√(" + fX.getTitle() + ")");
    this.equation = new VBox();
    this.fields = new HBox(15, field, fX.getHBox());
    this.fX = fX;
  }

  private Node getComponent() {
    return this.equation;
  }

  private void configureComponent() {
    this.equation.setAlignment(Pos.CENTER);
    this.equation.setSpacing(20);
    this.fields.setAlignment(Pos.CENTER);
  }

  public void composeComponent() {
    this.equation.getChildren().addAll(this.title, this.fields);
  }

  public Node build() {
    this.configureComponent();
    this.composeComponent();
    return this.getComponent();
  }

  public Root confirmClicked() {
    try {
      int n = 1;
      if (!this.field.getCharacters().isEmpty()) {
        n = Integer.parseInt(String.valueOf(this.field.getCharacters()));
      }
      MyFunction function = this.fX.confirmClicked();
      if (function == null) {
        this.equation.setStyle("-fx-control-inner-background: #b34d4d;");
      } else {
        this.equation.setStyle("-fx-control-inner-background: #49ba49;");
        return new Root(function, n);
      }
    } catch (NumberFormatException ignored) {
      this.equation.setStyle("-fx-control-inner-background: #b34d4d;");
    }
    return null;
  }
}
