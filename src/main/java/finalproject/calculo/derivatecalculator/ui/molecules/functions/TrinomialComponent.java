package finalproject.calculo.derivatecalculator.ui.molecules.functions;

import finalproject.calculo.derivatecalculator.common.FunctionComponent;
import finalproject.calculo.derivatecalculator.functions.Trinomial;
import finalproject.calculo.derivatecalculator.ui.atoms.AppTextField;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class TrinomialComponent implements FunctionComponent {
  private final TextField fieldA;
  private final TextField fieldN;
  private final TextField fieldB;
  private final TextField fieldM;
  private final TextField fieldC;
  private final HBox fields;
  private final String stringTitle;
  private final Label title;
  private final VBox equation;

  public TrinomialComponent() {
    this.fieldA = new AppTextField("a");
    this.fieldN = new AppTextField("n");
    this.fieldB = new AppTextField("b");
    this.fieldM = new AppTextField("m");
    this.fieldC = new AppTextField("C");
    this.fields = new HBox(15,
            this.fieldA,
            this.fieldN,
            this.fieldB,
            this.fieldM,
            this.fieldC
    );
    this.stringTitle = "ax^n + bx^m + C";
    this.title = new Label(this.stringTitle);
    this.equation = new VBox();
  }

  private Node getComponent() {
    return this.equation;
  }

  private void configureComponent() {
    this.equation.setAlignment(Pos.CENTER);
    this.equation.setSpacing(20);
  }

  public void composeComponent() {
    this.equation.getChildren().addAll(this.title, this.fields);
  }

  @Override
  public String getTitle() {
    return this.stringTitle;
  }

  @Override
  public HBox getHBox() {
    return this.fields;
  }

  public Node build() {
    this.configureComponent();
    this.composeComponent();
    return this.getComponent();
  }

  public Trinomial confirmClicked() {
    try {
      int numberA = 1;
      int numberN = 1;
      int numberB = 1;
      int numberM = 1;
      int numberC = 1;
      if (!this.fieldA.getCharacters().isEmpty()) {
        numberA = Integer.parseInt(String.valueOf(this.fieldA.getCharacters()));
      }
      if (!this.fieldN.getCharacters().isEmpty()) {
        numberN = Integer.parseInt(String.valueOf(this.fieldN.getCharacters()));
      }
      if (!this.fieldB.getCharacters().isEmpty()) {
        numberB = Integer.parseInt(String.valueOf(this.fieldB.getCharacters()));
      }
      if (!this.fieldM.getCharacters().isEmpty()) {
        numberM = Integer.parseInt(String.valueOf(this.fieldM.getCharacters()));
      }
      if (!this.fieldC.getCharacters().isEmpty()) {
        numberC = Integer.parseInt(String.valueOf(this.fieldC.getCharacters()));
      }
      this.equation.setStyle("-fx-control-inner-background: #49ba49;");
      return new Trinomial(numberA, numberN, numberB, numberM, numberC);
    } catch (NumberFormatException ignored) {
      this.equation.setStyle("-fx-control-inner-background: #b34d4d;");
    }
    return null;
  }
}
