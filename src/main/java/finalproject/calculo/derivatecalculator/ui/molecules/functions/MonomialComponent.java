package finalproject.calculo.derivatecalculator.ui.molecules.functions;

import finalproject.calculo.derivatecalculator.common.FunctionComponent;
import finalproject.calculo.derivatecalculator.functions.Monomial;
import finalproject.calculo.derivatecalculator.ui.atoms.AppTextField;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class MonomialComponent implements FunctionComponent {
  private final TextField field;
  private final HBox fields;
  private final String stringTitle;
  private final Label title;
  private final VBox equation;

  public MonomialComponent() {
    this.field = new AppTextField("C");
    this.fields = new HBox(this.field);
    this.stringTitle = "C";
    this.title = new Label(this.stringTitle);
    this.equation = new VBox();
  }

  private Node getComponent() {
    return this.equation;
  }

  private void configureComponent() {
    this.equation.setAlignment(Pos.CENTER);
    this.equation.setSpacing(20);
  }

  public void composeComponent() {
    this.equation.getChildren().addAll(this.title, this.field);
  }

  @Override
  public String getTitle() {
    return this.stringTitle;
  }

  @Override
  public HBox getHBox() {
    return this.fields;
  }

  public Node build() {
    this.configureComponent();
    this.composeComponent();
    return this.getComponent();
  }

  public Monomial confirmClicked() {
    try {
      int number = 1;
      if (!this.field.getCharacters().isEmpty()) {
        number = Integer.parseInt(String.valueOf(this.field.getCharacters()));
      }
      this.equation.setStyle("-fx-control-inner-background: #49ba49;");
      return new Monomial(number);
    } catch (NumberFormatException ignored) {
      this.equation.setStyle("-fx-control-inner-background: #b34d4d;");
    }
    return null;
  }
}
