package finalproject.calculo.derivatecalculator.ui.atoms;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class Layouts extends Button {
  public Layouts(String title, String equation) {
    VBox root = new VBox(5, new Label(title), new Label(equation));
    root.setAlignment(Pos.CENTER);
    setPrefSize(175, 50);
    setGraphic(root);
  }
}
