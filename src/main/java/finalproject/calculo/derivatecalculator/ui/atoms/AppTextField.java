package finalproject.calculo.derivatecalculator.ui.atoms;

import javafx.geometry.Pos;
import javafx.scene.control.TextField;

public class AppTextField extends TextField {
  public AppTextField(String prompt) {
    setPromptText(prompt);
    setAlignment(Pos.CENTER);
    setPrefHeight(30);
    setMaxWidth(45);
  }

}
