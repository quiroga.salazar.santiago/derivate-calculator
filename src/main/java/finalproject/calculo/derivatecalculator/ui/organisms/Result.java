package finalproject.calculo.derivatecalculator.ui.organisms;

import finalproject.calculo.derivatecalculator.helps.Equation;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Result {
  private final Stage stage;
  private final GridPane pane;
  private final VBox result;
  private final Button reset;
  private final Button close;
  private final HBox footer;

  public Result() {
    this.stage = new Stage();
    this.pane = new GridPane();
    this.result = new VBox(10, new Label("El Resultado es: "), new Label(Equation.result));
    this.reset = new Button("Reniciar");
    this.close = new Button("Cerrar");
    this.footer = new HBox(25, this.reset, this.close);

  }

  private Node getComponent() {
    return this.pane;
  }

  private void configureActions() {
    this.reset.setOnAction(e -> resetClicked());
    this.close.setOnAction(e -> closeClicked());
  }

  private void configureComponent() {
    this.pane.setAlignment(Pos.CENTER);
    this.pane.setPadding(new Insets(15));
    this.pane.setVgap(50);
    this.result.setAlignment(Pos.CENTER);
    this.footer.setAlignment(Pos.CENTER);
  }

  private void composeComponent() {
    this.pane.addRow(0, this.result);
    this.pane.addRow(1, this.footer);
  }

  private Node build() {
    this.configureActions();
    this.configureComponent();
    this.composeComponent();
    return new StackPane(this.getComponent());
  }

  public void showComponent() {
    this.stage.setTitle("Results");
    this.stage.setScene(new Scene((Parent) this.build(), 500, 500));
    this.stage.show();
  }

  private void resetClicked() {
    new Dashboard().showComponent();
    this.stage.close();
  }

  private void closeClicked() {
    this.stage.close();
  }
}
