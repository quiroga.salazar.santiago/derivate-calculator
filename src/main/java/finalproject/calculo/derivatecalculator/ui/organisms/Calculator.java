package finalproject.calculo.derivatecalculator.ui.organisms;

import finalproject.calculo.derivatecalculator.common.FunctionComponent;
import finalproject.calculo.derivatecalculator.common.MyFunction;
import finalproject.calculo.derivatecalculator.operations.Division;
import finalproject.calculo.derivatecalculator.operations.Multiplication;
import finalproject.calculo.derivatecalculator.operations.SumRest;
import finalproject.calculo.derivatecalculator.helps.Equation;
import finalproject.calculo.derivatecalculator.ui.molecules.functions.BinomialComponent;
import finalproject.calculo.derivatecalculator.ui.molecules.functions.MonomialComponent;
import finalproject.calculo.derivatecalculator.ui.molecules.functions.RootComponent;
import finalproject.calculo.derivatecalculator.ui.molecules.functions.TrinomialComponent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Calculator {
  private final Stage stage;
  private final GridPane pane;
  private final Button confirm;
  private final VBox body;
  private FunctionComponent fX;
  private FunctionComponent gX;
  private FunctionComponent root;

  public Calculator() {
    this.stage = new Stage();
    this.pane = new GridPane();
    this.confirm = new Button("Confirmar!");
    this.body = new VBox();
  }

  private Node getComponent() {
    return this.pane;
  }

  private void configureActions() {
    this.confirm.setOnAction(e -> confirmClicked());
  }

  private void configureComponent() {
    this.pane.setAlignment(Pos.CENTER);
    this.body.setAlignment(Pos.CENTER);
    this.body.setPadding(new Insets(15));
    this.body.setSpacing(15);
  }

  private void composeComponent() {
    if (Equation.fX.equals("Monomio")){
      this.fX = new MonomialComponent();
    } else if (Equation.fX.equals("Binomio")) {
      this.fX = new BinomialComponent();
    } else {
      this.fX = new TrinomialComponent();
    }
    if (!Equation.operation.equals("Raíz")) {
      if (Equation.gX.equals("Monomio")) {
        this.gX = new MonomialComponent();
      } else if (Equation.gX.equals("Binomio")) {
        this.gX = new BinomialComponent();
      } else {
        this.gX = new TrinomialComponent();
      }
      this.body.getChildren().addAll(
              new Label("F(x)"),
              fX.build(),
              new Label("G(x)"),
              gX.build(),
              this.confirm
      );
    } else {
      this.root = new RootComponent(this.fX);
      this.body.getChildren().addAll(
              new Label("F(x)"),
              root.build(),
              this.confirm
      );
    }
    this.pane.addRow(0, this.body);
  }

  private Node build() {
    this.configureActions();
    this.configureComponent();
    this.composeComponent();
    return new StackPane(this.getComponent());
  }

  public void showComponent() {
    this.stage.setTitle("My Derivate-Calculator");
    this.stage.setScene(new Scene((Parent) this.build(), 500, 750));
    this.stage.show();
  }

  private void confirmClicked() {
    if (Equation.operation.equals("Raíz")) {
      MyFunction fXFunction = this.root.confirmClicked();
      if (fXFunction != null) {
        Equation.result = fXFunction.getResult();
        new Result().showComponent();
        this.stage.close();
      }
    } else {
      MyFunction fXFunction = this.fX.confirmClicked();
      MyFunction gXFunction = this.gX.confirmClicked();
      if (fXFunction != null && gXFunction != null) {
        if (Equation.operation.equals("Muliplicación"))
          Equation.result = new Multiplication(fXFunction, gXFunction).getResult();
        else if (Equation.operation.equals("Suma / Resta"))
          Equation.result = new SumRest(fXFunction, gXFunction).getResult();
        else
          Equation.result = new Division(fXFunction, gXFunction).getResult();
        new Result().showComponent();
        this.stage.close();
      }
    }
  }
}
