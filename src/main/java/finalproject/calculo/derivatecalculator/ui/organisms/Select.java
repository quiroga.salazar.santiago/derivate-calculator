package finalproject.calculo.derivatecalculator.ui.organisms;

import finalproject.calculo.derivatecalculator.ui.atoms.Layouts;
import finalproject.calculo.derivatecalculator.helps.Equation;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Select {
  private final Stage stage;
  private final GridPane pane;
  private final HBox title;
  private final Layouts monomial;
  private final Layouts binomial;
  private final Layouts trinomial;
  private final String function;
  private final boolean isDouble;

  public Select(String function, boolean isDouble) {
    this.stage = new Stage();
    this.pane = new GridPane();
    this.title = new HBox(new Label("MyFunction " + function));
    this.monomial = new Layouts("Monomio", "C");
    this.binomial = new Layouts("Binomio", "ax + C");
    this.trinomial= new Layouts("Trinomio", "ax² + bx + C");
    this.function = function;
    this.isDouble = isDouble;
  }

  private Node getComponent() {
    return this.pane;
  }

  private void configureActions() {
    this.monomial.setOnAction(e -> monomialClicked());
    this.binomial.setOnAction(e -> binomialClicked());
    this.trinomial.setOnAction(e -> trinomialClicked());
  }

  private void configureComponent() {
    this.pane.setAlignment(Pos.CENTER);
    this.pane.setPadding(new Insets(15));
    this.pane.setVgap(25);
    this.title.setAlignment(Pos.CENTER);
  }

  private void composeComponent() {
    this.pane.addRow(0, this.title);
    this.pane.addRow(1, this.monomial);
    this.pane.addRow(2, this.binomial);
    this.pane.addRow(3, this.trinomial);
  }

  private Node build() {
    this.configureActions();
    this.configureComponent();
    this.composeComponent();
    return new StackPane(this.getComponent());
  }

  public void showComponent() {
    this.stage.setTitle("Chose your favorite option");
    this.stage.setScene(new Scene((Parent) this.build(), 400,400));
    this.stage.show();
  }

  private void monomialClicked() {
    if (this.function.equals("F(x)") && !isDouble) {
      Equation.fX = "Monomio";
      new Calculator().showComponent();
    } else if (this.function.equals("G(x)") && isDouble) {
      Equation.gX = "Monomio";
      new Calculator().showComponent();
    } else {
      Equation.fX = "Monomio";
    }
    this.stage.close();
  }

  private void binomialClicked() {
    if (this.function.equals("F(x)") && !isDouble) {
      Equation.fX = "Binomio";
      new Calculator().showComponent();
    } else if (this.function.equals("G(x)") && isDouble) {
      Equation.gX = "Binomio";
      new Calculator().showComponent();
    } else {
      Equation.fX = "Binomio";
    }
    this.stage.close();
  }

  private void trinomialClicked() {
    if (this.function.equals("F(x)") && !isDouble) {
      Equation.fX = "Trinomio";
      new Calculator().showComponent();
    } else if (this.function.equals("G(x)") && isDouble) {
      Equation.gX = "Trinomio";
      new Calculator().showComponent();
    } else {
      Equation.fX = "Trinomio";
    }
    this.stage.close();
  }
}
