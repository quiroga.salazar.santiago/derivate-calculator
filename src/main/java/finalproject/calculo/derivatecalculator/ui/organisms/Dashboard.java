package finalproject.calculo.derivatecalculator.ui.organisms;

import finalproject.calculo.derivatecalculator.ui.atoms.Layouts;
import finalproject.calculo.derivatecalculator.helps.Equation;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Dashboard {
  private final Stage stage;
  private final GridPane pane;
  private final HBox title;
  private final Layouts sum_rest;
  private final Layouts multiplication;
  private final Layouts division;
  private final Layouts root;

  public Dashboard() {
    this.stage = new Stage();
    this.pane = new GridPane();
    this.title = new HBox (new Label("Welcome to the Derivate Calculator"));
    this.sum_rest = new Layouts("Suma/Resta", "f(x) +- g(x)");
    this.multiplication = new Layouts("Multipliación", "f(x) * g(x)");
    this.division = new Layouts("División", "f(x) / g(x)");
    this.root = new Layouts("Raíz", "√f(x)");
  }

  private Node getComponent() {
    return this.pane;
  }

  private void configureActions() {
    this.sum_rest.setOnAction(e -> sumRestClicked());
    this.multiplication.setOnAction(e -> multiplicationClicked());
    this.division.setOnAction(e -> divisionClicked());
    this.root.setOnAction(e -> rootClicked());
  }

  private void configureComponent() {
    this.pane.setAlignment(Pos.CENTER);
    this.pane.setVgap(50);
    this.pane.setPadding(new Insets(50));
    this.title.setAlignment(Pos.CENTER);
  }

  private void composeComponent() {
    this.pane.addRow(0, this.title);
    this.pane.addRow(1, new HBox(25,
            this.sum_rest,
            this.multiplication
    ));
    this.pane.addRow(2, new HBox(25,
            this.division,
            this.root
    ));
  }

  private Node build() {
    this.configureActions();
    this.configureComponent();
    this.composeComponent();
    return new StackPane(this.getComponent());
  }

  public void showComponent() {
    this.stage.setTitle("My Derivate Calculator");
    this.stage.setScene(new Scene((Parent) this.build(), 500, 500));
    this.stage.show();
  }

  private void sumRestClicked() {
    new Select("G(x)", true).showComponent();
    new Select("F(x)", true).showComponent();
    Equation.operation = "Suma / Resta";
    this.stage.close();
  }

  private void multiplicationClicked() {
    new Select("G(x)", true).showComponent();
    new Select("F(x)", true).showComponent();
    Equation.operation = "Multiplicación";
    this.stage.close();
  }

  private void divisionClicked() {
    new Select("G(x)", true).showComponent();
    new Select("F(x)", true).showComponent();
    Equation.operation = "Division";
    this.stage.close();
  }

  private void rootClicked() {
    new Select("F(x)", false).showComponent();
    Equation.operation = "Raíz";
    this.stage.close();
  }
}
