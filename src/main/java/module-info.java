module calculator {
  requires javafx.controls;

  exports finalproject.calculo.derivatecalculator.ui.molecules.functions;
  exports finalproject.calculo.derivatecalculator.ui.organisms;
  exports finalproject.calculo.derivatecalculator;
  exports finalproject.calculo.derivatecalculator.helps;
}